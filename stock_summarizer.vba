Public Sub AggregateAllStocks()
    For Each ws In Sheets
        Worksheets(ws.Name).Activate
        Call StockAggregator
    Next ws
End Sub


Public Sub StockAggregator()
    Dim ticker As String
    Dim runDate As Long
    Dim volume As Double
    
    Dim currentTicker As String
    Dim outputRow As Long
    outputRow = 2
    Dim oldestRun As Range
    Dim newestRun As Range
    
    Dim sheet As Worksheet
    Set sheet = ActiveSheet
    Dim rows As Range
    
    Cells(1, 9).Value = "Ticker"
    Cells(1, 10).Value = "Yearly Change"
    Cells(1, 11).Value = "Percent Change"
    Cells(1, 12).Value = "Total Stock Volume"
    
    
    sheet.Columns("K").NumberFormat = "0.00%"
    Call SetupConditionalFormatting(sheet.Columns("K"))
    
    For Each r In sheet.rows
        If r.Row = 1 Then
            GoTo NextIteration
        End If
        
        ticker = sheet.Cells(r.Row, 1).Value
        runDate = sheet.Cells(r.Row, 2).Value
        If r.Row = 2 Then
            Set oldestRun = r
            Set newestRun = r
            currentTicker = ticker
        End If
        
         'If r.Row > 2265 Then
         '    Exit For
         'End If

        If ticker = currentTicker Then
                
            If runDate < oldestRun.Cells(2).Value Then
                Set oldestRun = r
            End If
            If runDate > newestRun.Cells(2).Value Then
                Set newestRun = r
            End If
            
            volume = volume + r.Cells(7).Value
            GoTo NextIteration
        Else
            Call RecordSummary(outputRow, currentTicker, newestRun, oldestRun, volume)
            outputRow = outputRow + 1
            currentTicker = ticker
            Set oldestRun = r
            Set newestRun = r
            volume = r.Cells(7).Value
        End If
NextIteration:
    Next r
    Call RecordSummary(outputRow, currentTicker, newestRun, oldestRun, volume)
    Call CreateSummaryTable
End Sub

Sub SetupConditionalFormatting(rg)
    Dim cond1 As FormatCondition
    Dim cond2 As FormatCondition
         
    Set cond1 = rg.FormatConditions.Add(xlCellValue, xlGreater, 0)
    Set cond2 = rg.FormatConditions.Add(xlCellValue, xlLess, 0)
     
    With cond1
    .Interior.Color = vbGreen
    End With
     
    With cond2
    .Interior.Color = vbRed
    End With
End Sub

Sub RecordSummary(outputRow, currentTicker, newestRun, oldestRun, totalVolume)
    Dim openPrice As Double
    Dim closePrice As Double
    Dim yearlyChange As Double
    
    openPrice = oldestRun.Cells(3).Value
    closePrice = newestRun.Cells(6).Value
    yearlyChange = closePrice - openPrice
    
    Cells(outputRow, 9).Value = currentTicker
    Cells(outputRow, 10).Value = yearlyChange
    
    If openPrice > 0 Then
        Cells(outputRow, 11).Value = yearlyChange / openPrice
    Else
        Cells(outputRow, 11).Value = 0
    End If
    
    Cells(outputRow, 12).Value = totalVolume
    
End Sub

Public Sub CreateSummaryTable()
    Cells(2, 15).Value = "Greatest % increase"
    Cells(3, 15).Value = "Greatest % Decrease"
    Cells(4, 15).Value = "Greatest total volume"
    
    Cells(1, 16).Value = "Ticker"
    Cells(1, 17).Value = "Value"
    Range("Q2:Q3").NumberFormat = "0.00%"
    
    Dim greatestIncrease As Double
    Dim greatestDecrease As Double
    Dim greatestVolume As Double
    
    Dim ticker As String
    Dim percentChange As Double
    Dim volume As Double
    
    Dim greatestIncreaseTicker As String
    Dim greatestDecreaseTicker As String
    Dim greatestVolumeTicker As String
    
    For Each r In ActiveSheet.Columns("I:L").rows
        
        If Cells(r.Row, 9).Value = "" Then
          Exit For
        End If
    
        If r.Row = 1 Then
            GoTo NextIteration
        End If
        
        If r.Row = 2 Then
            greatestIncreaseTicker = r.Cells(1)
            greatestIncrease = r.Cells(3)
            
            greatestDecreaseTicker = r.Cells(1)
            greatestDecrease = r.Cells(3)
            
            greatestVolumeTicker = r.Cells(1)
            greatestVolume = r.Cells(4)
            
            GoTo NextIteration
        End If
        
        ticker = r.Cells(1)
        percentChange = r.Cells(3)
        volume = r.Cells(4)
        
        If greatestIncrease < percentChange Then
            greatestIncrease = percentChange
            greatestIncreaseTicker = ticker
        End If
        
        If greatestDecrease > percentChange Then
            greatestDecrease = percentChange
            greatestDecreaseTicker = ticker
        End If
        
        If greatestVolume < volume Then
            greatestVolume = volume
            greatestVolumeTicker = ticker
        End If
                
NextIteration:
    Next r
    
    Cells(2, 16).Value = greatestIncreaseTicker
    Cells(2, 17).Value = greatestIncrease
    
    Cells(3, 16).Value = greatestDecreaseTicker
    Cells(3, 17).Value = greatestDecrease
    
    Cells(4, 16).Value = greatestVolumeTicker
    Cells(4, 17).Value = greatestVolume
End Sub


